import 'dart:async';
import 'package:flutter/material.dart';
import 'package:sekolahku/project/ListSiswa.dart';
import 'package:sekolahku/project/Login.dart';

class SplashScreen extends StatefulWidget{

  @override 
  _SplashScreenState createState() => _SplashScreenState();

}

class _SplashScreenState extends State<SplashScreen>{

  void initState(){
    super.initState();
    startSplashScreen();
  }

  startSplashScreen() async {
    var duration = const Duration(seconds: 5);
    return Timer(duration, (){
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (_) {
          return Login();
        })
      );
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(

      body: Center(
        
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "images/logo2.png",
              width: 150.0,
              height: 150.0,
            ),
            SizedBox(
              height: 24.0 
            ),
            Text(
              "SEKOLAHKU",
              style:  TextStyle(
                fontSize: 20, 
                fontWeight: FontWeight.bold, 
                color: Colors.grey
              ),
            ),
          ],
        ),
      ),
    );

  }
}